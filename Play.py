import pyaudio
import numpy as np


def play_sine(fsin, duration):
    pa = pyaudio.PyAudio()
    rate = 44100
    s = pa.open(output=True,
                channels=1,
                rate=rate,
                format=pyaudio.paInt16,
                output_device_index=2)

    t = np.linspace(0, duration, duration * rate)
    samples = (1000 * (np.sin(2 * np.pi * t * fsin))).astype(np.int16).tobytes()
    # play
    s.write(samples)


def get_config():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fsin', type=int, default=500)
    parser.add_argument('-t', '--duration', type=int, default=10)
    args = parser.parse_args()
    return args


def main():
    config = get_config()
    play_sine(config.fsin, config.duration)


if __name__ == '__main__':
    main()