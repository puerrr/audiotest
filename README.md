# AudioTest

Generate and test analog audio signals through Ursula and Rattlesnake.


## Usage

### Play.py

play an audio tone.

Usage: python Play.py -f 500 -t 3

eg. play tone at frequency 500 Hz for 3 sec

### spectrum.py

show waveform and spectrum from mic input.

Usage: python spectrum.py

